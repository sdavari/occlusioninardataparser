using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OcclusionInARParser
{
	class Program
	{
		static private string rootDir = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles";
		static private string path_renamed_files = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\all";
		static private string path_trialset_files = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\TrialSets\TrialSet";
		static private string csv_path = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\traialsetdata.csv";
		static private string csv_path_t = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\trials.csv";
		static private string csv_path_data = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\data.csv";
		static private string csv_path_saig_ans = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\saig_ans.csv";
		static private string csv_path_saig_total = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\saig_total.csv";
		static private string csv_path_saig_wave = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\saig_wave.csv";
		static private string csv_path_survey = @"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\saig_wave.csv";

		static private StringBuilder csv_content = new StringBuilder();
		static private StringBuilder csv_content_t = new StringBuilder();
		static private StringBuilder csv_data = new StringBuilder();
		static private StringBuilder csv_saig_total = new StringBuilder();
		static private StringBuilder csv_saig_ans = new StringBuilder();
		static private StringBuilder csv_saig_wave = new StringBuilder();
		static private StringBuilder csv_survey = new StringBuilder();
		const int NUM_PARTICIPANTS = 32;
		

		static void Main(string[] args)
		{

			// Rename files saved from experiment to include participant num in file name
			//rename_files();

			// Add the variable names row to the csv file
			string newline = "PID,Methods,Dataset,Accuracy,Time to answer,Time to wave back,Total number of clicks,Click used for replying,Clicks on occluding app,Number of extra wave backs";
			csv_content.AppendLine(newline);
			//  p, method, (int.Parse(trialset_num)  * 10 + t_num + 1), trial_clicks[t_num], trial_type, answer_time[t_num], waveback_time[t_num], !wrong_answers[t_num])
			newline = "PID,Methods,TrialID, # Clicks, Trial Type, Time to answer,Time to wave back,Right Answer";
			csv_content_t.AppendLine(newline);

			// Data
			newline = "PID,Methods,Trial_Type,Time_to_answer_Question_Only,Time_to_answer_Question_Only_Blocked,Time_to_answer_Question_Wave,Time_to_answer_Question_Wave_Blocked,Time_to_waveback_Wave_Only,Time_to_waveback_Question_Wave";
			csv_data.AppendLine(newline);

			// SAIG
			newline = "PID,Methods,Automation,Mechanism,Priority,Trial_Type,Answer_time,clicks,Accuracy";
			csv_saig_ans.AppendLine(newline);
			newline = "Methods,Automation,Mechanism,Priority,Trial_Type,Waveback_time,clicks,Accuracy";
			csv_saig_wave.AppendLine(newline);
			newline = "PID,Methods,Automation,Mechanism,Priority,Trial_Type,Ans_time,Waveback_Time,Total_time_QW,clicks,Accuracy";
			csv_saig_total.AppendLine(newline);


			//Survey
			newline = "PID,Methods,Ease-of-Info-Access,Ease-of-Finding-Kevin";
			csv_survey.AppendLine(newline);

			// Operations on files of each participant
			for (int p = 1; p <= NUM_PARTICIPANTS; p++)
			{
				add_participant_data_to_csv_file(p);
			}
			// Add all data to cvs file
			File.WriteAllText(csv_path, csv_content.ToString());
			File.WriteAllText(csv_path_t, csv_content_t.ToString());
			File.WriteAllText(csv_path_data, csv_data.ToString());
			File.WriteAllText(csv_path_saig_total, csv_saig_total.ToString());
			File.WriteAllText(csv_path_saig_ans , csv_saig_ans.ToString());


			// unify file data
			//unifyPre();
			//unifyInterview();


			// Keep Console Open
			Console.WriteLine("End! Press any Key to exit:");
			Console.ReadKey(); 
		}

		private static void unifyInterview()
		{
			string prefix = "final";
			var fileNames = Directory.EnumerateFiles(rootDir, prefix + "*", SearchOption.AllDirectories);
			var unifiedfiles = Directory.EnumerateFiles(path_renamed_files, "*.csv", SearchOption.AllDirectories);
			foreach (String path in fileNames)
			{
				var content = File.ReadAllText(path);
				content = content.ToLower();

				content = content.Replace("making the content transparent", "Transparency");
				content = content.Replace("moving the content", "Move");

				content = content.Replace("when the system automatically moved the content /made the content transparent.", "Automatic");
				content = content.Replace("when i manually moved the content /made the content transparent.", "manual");

				content = content.Replace("when the system started with transparent/ moved up glanceable content", "Real World prioritized");
				content = content.Replace("when the system started with opaque/ unmoved glanceable content", "Virtual prioritized");

				content = content.Replace("method 2: automatic-make transparent", "2");
				content = content.Replace("automatic-make transparent", "2");

				content = content.Replace("method 5: manual-make transparent-start transparent", "5");
				content = content.Replace("method 6: automatic-move", "6");
				content = content.Replace("method 1: manual-move-start from moved up", "1");

				File.WriteAllText(path, content);
				Console.WriteLine("Done unifying final interview file!");
				
			}
		}

		private static void unifyPre()
		{
			var content = File.ReadAllText(@"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\PreQuestionnaire.csv");
			content = content.ToLower();
			content = content.Replace("undergraduate student", "undergraduate");
			content = content.Replace("student-graduate", "graduate");
			content = content.Replace("undergrad student + intern", "undergraduate");
			content = content.Replace("graduate student", "graduate");
			content = content.Replace("junior", "undergraduate");
			content = content.Replace("undergrad student", "undergraduate");
			content = content.Replace("computer science; data analytics", "computer science");
			content = content.Replace("cs", "computer science");
			File.WriteAllText(@"D:\Personal_Folders\Shakiba\Fall19\Parser\OcclusionInARParser\inputfiles\unifiedPreQuestionnaire.csv", content);
		}

		private static void add_participant_data_to_csv_file(int p)
		{
			string prefix = p.ToString() + "_";
			var fileNames = Directory.EnumerateFiles(path_renamed_files, prefix + "*", SearchOption.AllDirectories);

			// Get each single file of the data for this participant
			foreach (String path in fileNames)
			{
				string[] lines = File.ReadAllLines(path);
				parse_file_to_csv_line(p, lines);
			}
		}

		private static void parse_file_to_csv_line(int p, string[] outputFile_lines)
		{
			// get Method and Trialset num for this file
			int v = int.Parse(outputFile_lines[0].Substring(1)) ;
			string method =  v.ToString();
			string trialset_num = outputFile_lines[1].Substring(1);
			string automatiom = "";
			string mechanism = "";
			string priority = "";
			
			// get initial trialset file 
			string[] trialsetData = File.ReadAllLines(path_trialset_files + trialset_num.ToString().Substring(1) + ".txt");

			// initiate arrays of trial data
			//may need later
			//int[] app_num = new int[10];
			int num_question_only_trials = 0,
				num_wave_only_trials = 0,
				num_question_wave_trials = 0;
			int[] blocked_app = new int[10];
			int[] questioned_app = new int[10];

			//times for one trial
			int answer = 50;
			float time_start_trial = 0.0f;
			float time_asked = 0.0f;
			float time_waved = 0.0f;
			float time_answered = 0.0f;
			float time_waved_back = 0.0f;

			// Metrics
			int num_trial_being_checked = 0;
			float accuracy = 0.0f;
			string[] trial_type = new string[10];
			bool[] wrong_answers = new bool[10]  ;
			int total_click_count = 0;
			int[] trial_clicks = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int[] responce_click_count = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int click_count_on_blocked = 0;
			double[] waveback_time = new double[10];
			double[] answer_time = new double[10];
			int extra_wave_backs_count = 0;

			// get info of the specific trial
			for (int trial_num = 0; trial_num < 10; trial_num++)
			{
				int start_ind = 5 * trial_num;
				//answer[trial_num] = int.Parse(trialsetData[start_ind + 1]);
				//may need later
				//app_num[trial_num] = int.Parse(trialsetData[start_ind]);
				blocked_app[trial_num] = int.Parse(trialsetData[start_ind + 4]);
			}

			// estimate each trial separately
			foreach (string line in outputFile_lines)
			{
				if (line.Substring(0, 1) == "N")
				{
					if (num_trial_being_checked != 0)
					{
						if (time_start_trial < time_answered && time_start_trial < time_waved_back)
						{
							if(questioned_app[num_trial_being_checked-1] == blocked_app[num_trial_being_checked-1])
								trial_type[num_trial_being_checked - 1] = "awb";
							else
								trial_type[num_trial_being_checked - 1] = "aw";
							num_question_wave_trials++;
							waveback_time[num_trial_being_checked - 1] = time_waved_back - time_waved;
							answer_time[num_trial_being_checked - 1] = time_answered - time_asked;
						}
						else if (time_start_trial < time_waved_back) // there has been only a wave in trial
						{
							trial_type[num_trial_being_checked - 1] = "w";
							num_wave_only_trials++;
							waveback_time[num_trial_being_checked - 1] = time_waved_back - time_waved;
							answer_time[num_trial_being_checked - 1] = 0.0f;
						}
						else // there has been only a question in trial
						{
							if (questioned_app[num_trial_being_checked - 1] == blocked_app[num_trial_being_checked - 1])
								trial_type[num_trial_being_checked - 1] = "ab";
							else
								trial_type[num_trial_being_checked - 1] = "a";
							num_question_only_trials++;
							answer_time[num_trial_being_checked - 1] = time_answered - time_asked;
							waveback_time[num_trial_being_checked - 1] = 0.0f;
						}

						//Note: Adding zero for trials that do not ask Q or do not have wave
					}
					num_trial_being_checked++;
					time_answered = 0;
					time_asked = 0;
					time_start_trial = float.Parse(line.Substring(2, line.Length - 2));
				}
				if (num_trial_being_checked != 0 && num_trial_being_checked < 11)
				{
					switch (line.Substring(0, 1))
					{
						case "Q":
							time_asked = float.Parse(line.Substring(6, line.Length - 6));
							answer = int.Parse(line.Substring(4, 1));
							questioned_app[num_trial_being_checked -1] = int.Parse(line.Substring(2, 1));
							break;
						case "W":
							time_waved = float.Parse(line.Substring(2, line.Length - 2));
							break;
						case "B":
							if (time_waved < time_start_trial) // there has been no waving in this trial
							{
								extra_wave_backs_count++;
							}
							else
								time_waved_back = float.Parse(line.Substring(2, line.Length - 2));
							break;
						case "A":
							if (int.Parse(line.Substring(2, 1)) != answer)
								wrong_answers[num_trial_being_checked - 1] = true;
							else
								wrong_answers[num_trial_being_checked - 1] = false;
							time_answered = float.Parse(line.Substring(4, line.Length - 4));
							answer = 50;
							break;
						case "C":
							// Do we wanna count all clicks or just the ones done during q and A in trials?
							total_click_count++;
							trial_clicks[num_trial_being_checked - 1]++;
							int clicekd_on = int.Parse(line.Substring(2, 1));
							if (clicekd_on == blocked_app[num_trial_being_checked - 1])
							{
								click_count_on_blocked++;
							}
							if (time_waved_back < time_waved || time_answered < time_asked)  // if question is not answered or wave is not waved back
							{
								responce_click_count[num_trial_being_checked - 1]++;
							}
							break;
						default:
							break;

					}
				}
				/* 
				 * Do not count the clicks that happened before first trial started
				else if (line.Substring(0, 1) == "C")
				{
					total_click_count++;
				}
				*/
			}
			accuracy = (10.0f - wrong_answers.Where(c=>true).Count() )/ 10.0f;

			// assign methods name and dimensions 
			switch (method)
			{
				case "1":
					
					method = "Manual_Move_RW Prioritized";
					automatiom = "Manual";
					mechanism = "Move";
					priority = "RW Prioritized";
					break;
				case "2":
					method = "Automatic Act_Make Transparent";
					automatiom = "Automatic Act";
					mechanism = "Make Transparent";
					priority = "Virtual Prioritized";
					break;
				case "3":
					method = "Automatic Detect_Make Transparent";
					automatiom = "Automatic Detect";
					mechanism = "Make Transparent";
					priority = "Virtual Prioritized";
					break;
				case "4":
					method = "Manual_Move_Virtual Prioritized";
					automatiom = "Manual";
					mechanism = "Move";
					priority = "Virtual Prioritized";
					break;
				case "5":
					method = "Manual_Make Transparent_RW Prioritized";
					automatiom = "Manual";
					mechanism = "Make Transparent";
					priority = "RW Prioritized";
					break;
				case "6":
					method = "Automatic Act_Move";
					automatiom = "Automatic Act";
					mechanism = "Move";
					priority = "Virtual Prioritized";
					break;
				case "7":
					method = "Manual_Make Transparent_Virtual Prioritized";
					automatiom = "Manual";
					mechanism = "Make Transparent";
					priority = "Virtual Prioritized";
					break;
				case "8":
					method = "Automatic Detect_Move";
					automatiom = "Automatic Detect";
					mechanism = "Move";
					priority = "Virtual Prioritized";
					break;
				default:
					break;

			}

			/*
			// Normalize the times
			double[] normalized_answer_time = answer_time;
			double dataMax = normalized_answer_time.Max();
			double dataMin = normalized_answer_time.Min();
			double mu = normalized_answer_time.Sum() / 10.0f;
			double range = dataMax - dataMin;
			for (int d = 0; d < 10; d++)
			{
				normalized_answer_time[d] -= mu;
				normalized_answer_time[d] /= range;
			}

			double[] normalized_waveback_time = waveback_time;
			dataMax = waveback_time.Max();
			dataMin = waveback_time.Min();
			mu = normalized_waveback_time.Sum() / 10.0f;
			range = dataMax - dataMin;
			for (int d = 0; d < 10; d++)
			{
				normalized_waveback_time[d] -= mu;
				normalized_waveback_time[d] /= range;
			}
			*/

			// Add data of this file as a line to the csv file
			// "PID,Methods,Dataset,Accuracy,Time to answer,Time to wave back,Total number of clicks,Click used for replying,Clicks on occluding app,Number of extra wave backs"
			string newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", p, method, trialset_num, accuracy, answer_time.Average(), waveback_time.Average(), total_click_count, responce_click_count, click_count_on_blocked, extra_wave_backs_count);
			csv_content.AppendLine(newline);




			
			// Add data of each trial as one line to the csv file
			//"PID,Methods,Dataset,Trial,Time to answer,Time to wave back,Accuracy,Total number of clicks,Click used for replying,Clicks on occluding app,Number of extra wave backs"
			for (int t_num = 0; t_num < 10; t_num++)
			{
				newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", p, method, (int.Parse(trialset_num)  * 10 + t_num + 1), trial_clicks[t_num], trial_type[t_num], answer_time[t_num], waveback_time[t_num], !wrong_answers[t_num] ? 1 : 0 );
				csv_content_t.AppendLine(newline);
				
				/*
				if (trial_type[t_num] == "a" || trial_type[t_num] == "ab" || trial_type[t_num] == "aw" || trial_type[t_num] == "awb")//ans
				{
					newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", method, automatiom, mechanism, priority, trial_type[t_num], answer_time[t_num] , trial_clicks[t_num], !wrong_answers[t_num]);
					csv_saig_ans.AppendLine(newline);
				}
				if (trial_type[t_num] == "w" || trial_type[t_num] == "aw" || trial_type[t_num] == "awb") //wave
				{
					newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", method, automatiom, mechanism, priority, trial_type[t_num], waveback_time[t_num], trial_clicks[t_num], !wrong_answers[t_num]);
					csv_saig_wave.AppendLine(newline);
				}*/

			}


			// For each method: for w ones , for a ones, for aw ones times
			
			  
			/*float a_a = 0.0f,
				a_aw = 0.0f,
				a_awb = 0.0f,
				a_ab = 0.0f,
				w_aw = 0.0f, 
				w_w = 0.0f;
			int a_count = 0,
				ab_count = 0,
				w_count = 0,
				aw_count = 0,
				awb_count = 0;
			for (int t_num = 0; t_num < 10; t_num++)
			{
				switch (trial_type[t_num])
				{
					case "a":
						{
							a_count++;
							a_a += (float)answer_time[t_num];
							break;
						}
					case "ab":
						{
							ab_count++;
							a_ab += (float)answer_time[t_num];
							break;
						}
					case "w":
						{
							w_count++;
							w_w += (float)waveback_time[t_num];
							break;
						}
					case "aw":
						{
							aw_count++;
							a_aw += (float)answer_time[t_num];
							w_aw += (float)waveback_time[t_num];
							break;
						}
					case "awb":
						{
							awb_count++;
							a_awb += (float)answer_time[t_num];
							w_aw += (float)waveback_time[t_num];
							break;
						}
				}
			}
			*/

			double[] task_time = new double[5] { 0, 0, 0, 0, 0 };
			double[] trial_Type_clicks = new double[5] { 0, 0, 0, 0, 0 };
			int[] num_trials_for_type = new int[5] { 0,0,0,0,0};

			//static readonly string[] trial_types = new string[5] { "a", "w", "aw", "awb", "ab" };
			for (int t_num = 0; t_num < 10; t_num++)
			{
				switch (trial_type[t_num])
				{
					case "a":
						{
							task_time[0] += answer_time[t_num];
							trial_Type_clicks[0] += trial_clicks[t_num];
							num_trials_for_type[0]++;
							break;
						}
					case "w":
						{
							task_time[1] += waveback_time[t_num];
							trial_Type_clicks[1] += trial_clicks[t_num];
							num_trials_for_type[1]++;
							break;
						}
					case"aw":
						{
							task_time[2] += Math.Max(answer_time[t_num], waveback_time[t_num]);
							trial_Type_clicks[2] += trial_clicks[t_num];
							num_trials_for_type[2]++;
							break;
						}
					case "awb":
						{
							task_time[3] += Math.Max(answer_time[t_num], waveback_time[t_num]);
							trial_Type_clicks[3] += trial_clicks[t_num];
							num_trials_for_type[3]++; ;
							break;
						}
					case "ab":
						{
							task_time[4] += answer_time[t_num];
							trial_Type_clicks[4] += trial_clicks[t_num];
							num_trials_for_type[4]++;
							break;
						}
				}
			}

			for (int type_num = 0; type_num <5; type_num++)
			{
				string t_type = "";
				switch (type_num)
				{
					case 0:
						t_type = "a";
						break;
					case 1:
						t_type = "w";
						break;
					case 2:
						t_type = "aw";
						break;
					case 3:
						t_type = "awb";
						break;
					case 4:
						t_type = "ab";
						break;	
				}
				task_time[type_num] /= (float)num_trials_for_type[type_num];
				trial_Type_clicks[type_num] /= (float)num_trials_for_type[type_num];
				//PID,Methods,Trial_Type,Time_to_answer_Question_Only,Time_to_answer_Question_Only_Blocked,Time_to_answer_Question_Wave,Time_to_answer_Question_Wave_Blocked,Time_to_waveback_Wave_Only,Time_to_waveback_Question_Wave"
				//newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", p, method, t_type, a_a / (float)(a_count), a_ab / (float)(ab_count), a_aw / (float)aw_count, a_awb / (float)awb_count, w_w / (float)w_count, w_aw / (float)(aw_count + awb_count));
				//csv_data.AppendLine(newline);

				//SAIG
				//double QW_time = 0.0f;
				//if (trial_type[t_num] == "aw" || trial_type[t_num] == "awb")
				//	QW_time = normalized_answer_time[t_num] + normalized_waveback_time[t_num];
				//PID, Methods,Automation,Mechanism,Priority,Trial_Type,Ans_time,Waveback_Time,Total_time_QW,clicks,Accuracy
				//newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", p, method, automatiom, mechanism, priority, trial_type[t_num], answer_time[t_num], waveback_time[t_num], QW_time, trial_clicks[t_num], !wrong_answers[t_num] ? 1 : 0);
				newline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", p, method, automatiom, mechanism, priority, t_type , task_time[type_num] , trial_Type_clicks[type_num]);
				csv_saig_ans.AppendLine(newline);
			}
			
		}

		private static void rename_files()
		{
			int ts = 1;
			int participant_num = 1;
			string prefix = "text-";
			var fileNames = Directory.EnumerateFiles( rootDir, prefix + "*", SearchOption.AllDirectories);
			var renamedFiles = Directory.EnumerateFiles(path_renamed_files, "*.txt", SearchOption.AllDirectories);
			if ((renamedFiles.LongCount() == 0) && (fileNames.LongCount() != 0))
			{
				foreach (String path in fileNames)
				{
					string dir = Path.GetDirectoryName(path);
					string fileName = Path.GetFileName(path);
					string newPath = Path.Combine(path_renamed_files, participant_num.ToString() + "_" + (ts++).ToString() + ".txt");
					File.Copy(path, newPath);
					if (ts == 9)
					{
						participant_num++;
						ts = 1;
					}
				}
				Console.WriteLine("Done renaming files!");
			}
			else
				Console.WriteLine("Files already renamed");
		}

	}
}
